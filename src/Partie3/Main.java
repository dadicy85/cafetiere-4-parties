package Partie3;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

	public static void main(String[] args) {
		
		Restaurant restaurant1 = new Restaurant("Latte sur les rochers"); 
		Restaurant restaurant2 = new Restaurant("Une Tasse de joie"); 
		Restaurant restaurant3 = new Restaurant(); 
		
		List<Restaurant> restaurants = new ArrayList<>();
		restaurants.add(restaurant1); 
		restaurants.add(restaurant2); 
		restaurants.add(restaurant3); 
		
		List<Client> listeClient1 = creerClienList(20);
		List<Client> listeClient2 = creerClienList(20);
		List<Client> listeClient3 = creerClienList(20);

		List<Client> listeClientsExpulse = new ArrayList<>(); 
		
		servirClients(restaurants,listeClient1, listeClientsExpulse); 
		servirClients(restaurants,listeClient2, listeClientsExpulse); 
		servirClients(restaurants,listeClient3, listeClientsExpulse); 
	
		restaurant1.resultat(); 
		restaurant2.resultat(); 
		restaurant3.resultat(); 
		
		System.out.println("Liste des mauvais clients:");
		for(Client client : listeClientsExpulse){
			System.out.println("Prénom: " + client.nom);
		}
	}
	
	public static void servirClients(List<Restaurant> restaurants, 
			                         List<Client> listeClient, 
			                         List<Client> listeClientsExpulse){
		
		Random random = new Random();
		
		for(Client client: listeClient){
			
			Restaurant choixResto = restaurants.get(random.nextInt(restaurants.size())); 
			double argent = choixResto.servir(client); 
			
			if(argent > 0){
				choixResto.listeClientServi.add(client);
			} else {
				listeClientsExpulse.add(client); 
			}
		}
	}
	
	static List<Client> creerClienList(int nombre){
		
		List<Client> listClient = new ArrayList<>();
		for(int i = 0; i < nombre; i++){
			listClient.add(genereClient()); 
		}
		return listClient; 
	}
	
	static Client genereClient(){
		
		Random random = new Random(); 
		
		String nom = BanqueDeDonne.listeNoms.get(random.nextInt(BanqueDeDonne.listeNoms.size())) ; 
		Cafe cafe = BanqueDeDonne.listeCommandes.get(random.nextInt(BanqueDeDonne.listeCommandes.size())) ; 
		Tasse tasse = BanqueDeDonne.listeTasses.get(random.nextInt(BanqueDeDonne.listeTasses.size())) ; 
				
		return new Client(nom, cafe, tasse); 
	}
}
