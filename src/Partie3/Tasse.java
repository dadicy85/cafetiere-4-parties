package Partie3;


public class Tasse {

	double quantiteCafeMax;
	Cafe cafe;

	Tasse() {

		this.quantiteCafeMax = 100;
	}

	Tasse(double quantiteCafeMax) {

		this.quantiteCafeMax = quantiteCafeMax;

	}

	double boire(double quantiteBue) {
        cafe.quantiteLiquideMl = cafe.quantiteLiquideMl - quantiteBue;
        return cafe.quantiteLiquideMl;
	}
	
	void boire() {
		cafe.quantiteLiquideMl = 0;
	}
}
