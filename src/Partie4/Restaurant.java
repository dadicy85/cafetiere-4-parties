package Partie4;

import java.util.ArrayList;


public class Restaurant {
	
	Cafetiere cafetiere;
	double profit;
	ArrayList<Client> listeClientServi;
	String nom;
	
    public Restaurant(String nom) { 
    	
    	
    	this.nom = nom;
		this.cafetiere = new Cafetiere();
		listeClientServi = new ArrayList<Client>();
	}
    
    public Restaurant() { 
		
    	this("Le Restaurant");	
	}

	double servir(Client client) {
		
		double valeurFacture = 0;

		if(client.commandeCafe == null || client.commandeCafe.typeCafe == TypeCafe.BATARD) {
			valeurFacture = 0;
			System.out.println("Va t'en de mon établissement !!");
			return valeurFacture;
		}
		
		if(client.tasse == null) {
			if(client.commandeCafe.quantiteLiquideMl <= 100) {
				valeurFacture += 2;
				client.tasse = new Tasse();
			} else {
				valeurFacture += 3;
				client.tasse = new Tasse(500);
			}
		}
				
		valeurFacture += client.commandeCafe.typeCafe.coutParMl * client.commandeCafe.quantiteLiquideMl;
		
		cafetiere.remplirTasse(client.tasse, client.commandeCafe.typeCafe, client.commandeCafe.quantiteLiquideMl);
		
		profit += valeurFacture;
		client.valeurFacture = valeurFacture;
		return valeurFacture;
	    }	
	    
	    public void resultat(){
		    System.out.println("Le restaurant nommé: " + nom + ", a fait " + profit + " € de profit en ayant servi " + listeClientServi.size() + " clients.");
	    }
}
