package Partie4;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class Main {

public static void main(String[] args) {
    
	// Client
	 JFrame frame = new JFrame();
	    String nom = JOptionPane.showInputDialog(frame, "Bonjour ! Quel est votre nom ?");   
    
    // Commande
    JOptionPane.showMessageDialog(frame, "Que voulez vous commander " + nom + " ? ");
    TypeCafe[] options = { TypeCafe.BOURBON, TypeCafe.JAVA, TypeCafe.MOKA, TypeCafe.TYPICA, TypeCafe.BATARD };
    TypeCafe choix = (TypeCafe) JOptionPane.showInputDialog(frame, "Quel type de café vous désirez ?", 
    		"Nos choix :",
            JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
    if (choix == TypeCafe.BATARD ) {
        JOptionPane.showMessageDialog(frame, "Hors de mon établissement !!!");
    } else {
    	JOptionPane optionPane = new JOptionPane();
        JSlider slider = getSlider(optionPane);
        optionPane.setMessage(new Object[] { 
        		"Quelle quantité vous désirez ? : ", slider 
        		});
        optionPane.setMessageType(JOptionPane.QUESTION_MESSAGE);
        optionPane.setOptionType(JOptionPane.YES_NO_OPTION);
        JDialog dialog = optionPane.createDialog(frame, "Quantité de café");
        dialog.setVisible(true);
        double quantite = (Integer) optionPane.getInputValue();
        double valeurFacture = (double)quantite * getPrix(choix);
        
        // Paiement
        int tentative = 0;
        int nombreMaxTentatives = 3;
        boolean paiementReussi = false;
        while (!paiementReussi && tentative < nombreMaxTentatives) {	
            double paiement = Double.parseDouble(JOptionPane.showInputDialog("Le total est de " + valeurFacture + "€. Entrez le paiement en centimes :")) / 100;
            if (paiement == valeurFacture) {
                JOptionPane.showMessageDialog(null, "Merci pour votre paiement !");
                paiementReussi = true;
            } else if (paiement > valeurFacture) {
                double monnaie = paiement - valeurFacture;
                JOptionPane.showMessageDialog(null, "Merci pour votre paiement ! Votre monnaie est de " + monnaie + "€.");
                paiementReussi = true;
            } else {
                tentative++;
                if (tentative >= nombreMaxTentatives) {
                    JOptionPane.showMessageDialog(null, "Au voleur, au voleur ! Mais que fait la police ?");
                }
            }
        }
    }
}
private static JSlider getSlider(final JOptionPane optionPane) {
	optionPane.setInputValue(100);
	JSlider slider = new JSlider(0, 500, 100);
	slider.setMajorTickSpacing(500);
	slider.setMinorTickSpacing(50);
	slider.setPaintTicks(true);
	slider.setPaintLabels(true);
	ChangeListener changeListener = new ChangeListener() {
		@Override
		public void stateChanged(ChangeEvent e) {
			JSlider theSlider = (JSlider) e.getSource();	
			if(!theSlider.getValueIsAdjusting()) {
				optionPane.setInputValue((int) theSlider.getValue());
			}
		}
	};
	slider.addChangeListener(changeListener);
	return slider;
}

private static double getPrix(TypeCafe type) {
    switch(type) {
        case JAVA: return 0.035;
        case MOKA: return 0.025;
        case TYPICA: return 0.027;
        case BOURBON: return 0.030;
        default: return 0;
    }
}
}