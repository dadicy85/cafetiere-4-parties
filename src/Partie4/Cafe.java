package Partie4;

public class Cafe {

	TypeCafe typeCafe;
	double quantiteLiquideMl;

	Cafe() {

		this.typeCafe = TypeCafe.MOKA;
		this.quantiteLiquideMl = 100;
	}

	Cafe(TypeCafe TypeCafe, double quantiteLiquideMl) {

		this.typeCafe = TypeCafe;
		this.quantiteLiquideMl = quantiteLiquideMl;

	}
}
