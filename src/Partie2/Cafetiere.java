package Partie2;

public class Cafetiere {

	void remplirTasse(Tasse tasse) {
		tasse.cafe = new Cafe(TypeCafe.MOKA, tasse.quantiteCafeMax);
	}

	void remplirTasse(Tasse tasse, TypeCafe typeCafe, double quantite) {
		
		if(tasse.cafe != null) {
			tasse.cafe.quantiteLiquideMl += quantite;
			if(typeCafe != tasse.cafe.typeCafe)
				tasse.cafe.typeCafe = TypeCafe.BATARD;
		}
		else
			tasse.cafe = new Cafe(typeCafe, quantite) ;
			
		
		if(tasse.cafe.quantiteLiquideMl > tasse.quantiteCafeMax) {
			System.out.println("Mon café déborde !!");
			tasse.cafe.quantiteLiquideMl = tasse.quantiteCafeMax;
		}
	}
  }
