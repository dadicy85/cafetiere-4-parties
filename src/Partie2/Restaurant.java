package Partie2;

public class Restaurant {
	
	Cafetiere cafetiere;
	double profit;
	
    public Restaurant() { 
		this.cafetiere = new Cafetiere();
	}

	double servir(Client client) {
		
		double valeurFacture = 0;

		if(client.commandeCafe == null || client.commandeCafe.typeCafe == TypeCafe.BATARD) {
			valeurFacture = 0;
			System.out.println("Va t'en de mon établissement !");
			return valeurFacture;
		}
		
		if(client.tasse == null) {
			if(client.commandeCafe.quantiteLiquideMl <= 100) {
				valeurFacture += 2;
				client.tasse = new Tasse();
			} else {
				valeurFacture += 3;
				client.tasse = new Tasse(500);
			}
		}
				
		valeurFacture += client.commandeCafe.typeCafe.coutParMl * client.commandeCafe.quantiteLiquideMl;
		
		cafetiere.remplirTasse(client.tasse, client.commandeCafe.typeCafe, client.commandeCafe.quantiteLiquideMl);
		
		profit += valeurFacture;
		client.valeurFacture = valeurFacture;
		return valeurFacture;
	}	
}
