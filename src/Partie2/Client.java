package Partie2;

public class Client {

	Tasse tasse;
	Cafe commandeCafe;
	String nom;
	double valeurFacture;
	
	public Client(String nom, Cafe commandeCafe, boolean avecTasse) {
		
		this.nom = nom;
				
		if(avecTasse)
			this.tasse = new Tasse(100);
		this.commandeCafe = commandeCafe ;
	}
		
	public Client(String nom, Cafe commandeCafe, Tasse tasse) {
		this.nom = nom ; 
		this.tasse = tasse ; 
		this.commandeCafe = commandeCafe ; 
	}
	
    public Client() {
		
		this.tasse = new Tasse();
		this.commandeCafe = null;
		this.nom = "Jean";
	}
}
